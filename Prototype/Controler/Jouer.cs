﻿using Prototype.Model;
using Prototype.Service;
using Prototype.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Prototype.Controler
{
    public class Jouer
    {
        private static Personne personne = null;
        private static Partie partie = null;
        private static Carte[] les2Cartes = new Carte[2];
        private static Carte uneCarte = null;
        private static int nbreClic = 0;
        private static string[] lesImages = null;
        private static string lImageDos = "/Ressources/image0.jpg";

        public static Personne GetPersonne()
        {
            return personne;
        }
        public static void SetPersonne(Personne laPersonne)
        {
            personne = laPersonne;
        }
        public static Partie GetPartie()
        {
            return partie;
        }
        public static Carte[] GetLes2Cartes()
        {
            return les2Cartes;
        }

        public static void Initialiser(int x)
        {
            
           Niveau[] lesNiveaux = new Niveau[3]
            {
                new Niveau(1,4,2,300),
                new Niveau(2,16,4,150),
                new Niveau(3,36,6,100)
            };
            int total = lesNiveaux[x].GetNbreTotalCarte() / 2;

           Carte[] lesCartesModeles = new Carte[total];

            lesImages = new string[37];

            for(int i = 1; i < 37; i++)
            {
                lesImages[i] = "/Ressources/image" + i + ".jpg";
            }

            lesImages = LesServices<string>.MelangerLesCartes(lesImages);

            for (int i = 0; i < total; i++)
            {
                lesCartesModeles[i] = new Carte(lImageDos, lesImages[i]);
            }

            partie = new Partie(lesNiveaux[x], personne, lesCartesModeles);
        }

        public static bool VerificationJeu()
        {
            bool resultat = true;

            foreach(Carte elem in partie.GetLesCartes())
            {
                if(elem.GetStatus()== false)
                {
                    resultat = false;
                }
            }

            if (resultat == true)
            {
                VerifierUnTour();
            }
            return resultat;
        }

        public static void EnregistrerNbreClicParti()
        {
            partie.SetNbreClics(nbreClic);
        }
        public static void CliquerUneCarte()
        {
            uneCarte.SetStatus(true);
            uneCarte.ApparaillerBouton();
        }


        public static bool JouerUnTour(Carte laCarte)
        {
            bool resultat = false;
            uneCarte = laCarte;

            if (uneCarte.GetStatus() == false)
            {
                nbreClic++;

                if (les2Cartes[0] == null)
                {
                    les2Cartes[0] = uneCarte;
                    CliquerUneCarte();
                }
                else
                {
                    les2Cartes[1] = uneCarte;
                    CliquerUneCarte();

                    resultat = true;
                }
            }
            if(resultat) {
                VerifierUnTour();
            }
            return resultat;
        }

        public static void VerifierUnTour()
        {
            if ((les2Cartes[0] != null) && (les2Cartes[1] != null))
            {
                if (les2Cartes[0].GetValeur()!= les2Cartes[1].GetValeur())
                {
                    MessageBoxTemp.Show("ok","ok",1000);
                    InitialiserUnTourPerdu();
                }
                MiseAZeroDesCartes();
                if (VerificationJeu() == true)
                {
                    MessageBox.Show("Le jeu est terminé. Vous avez gagné en " + nbreClic + " clics");
                    EnregistrerNbreClicParti();
                }
            }
        }
        public static void MiseAZeroDesCartes()
        {
            uneCarte = null;
            les2Cartes[1] = null;
            les2Cartes[0] = null;
        }
        public static void InitialiserUnTourPerdu()
        {
            for(int i = 0; i < 2; i++)
            {
                les2Cartes[i].SetStatus(false);
                les2Cartes[i].ApparaillerBouton();
            }
        }

        
    }
}
