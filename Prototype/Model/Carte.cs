﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using Prototype.Service;

namespace Prototype.Model
{
    public class Carte
    {
        private static int nbreCarte = 0;
        private static int numero = 0;
        private Button bouton;
        private bool status = false;
        private int valeur = 0;
        private string cheminImageDos;
        private string cheminImageFace;
        private Image imageDos;
        private Image imageFace;
        private string titre;
        private int numColFenetre = 0;
        private int numRangFenetre = 0;
        private int identifiant = 0;

        public Carte(string leCheminImageDos, string leCheminImageFace)
        {
            nbreCarte++;
            this.valeur = nbreCarte;
            this.cheminImageDos = leCheminImageDos;
            this.cheminImageFace = leCheminImageFace;
            this.titre = $"Carte Modèle n°{nbreCarte}";
            this.imageDos = Service.LesServices<Carte>.CreerUneImage(leCheminImageDos);
            this.imageFace = Service.LesServices<Carte>.CreerUneImage(leCheminImageFace);
            this.bouton = new Button();
            ApparaillerBouton();

        }
        public Carte(Carte laCarte)
        {
           
            this.valeur = laCarte.GetValeur();
            this.imageDos = LesServices<Carte>.CreerUneImage(laCarte.GetCheminImageDos());
            this.imageFace = LesServices<Carte>.CreerUneImage(laCarte.GetCheminImageFace());
            this.titre = $"Carte copie n°{laCarte.GetValeur()}";
            this.bouton = new Button();
            ApparaillerBouton();
        }
        public Button GetBouton()
        {
            return this.bouton;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public void SetStatus(bool leStatus)
        {
            this.status = leStatus;
        }
        public int GetValeur()
        {
            return this.valeur;
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public int GetNumColFenetre()
        {
            return this.numColFenetre;
        }
        public void SetNumColFenetre(int numCol)
        {
            this.numColFenetre = numCol;
        }
        public int GetNumRangFenetre()
        {
            return this.numRangFenetre;
        }
        public void SetNumRangFenetre(int numRang)
        {
            this.numRangFenetre = numRang;
        }
        public string GetCheminImageDos()
        {
            return this.cheminImageDos;
        }
        public string GetCheminImageFace()
        {
            return this.cheminImageFace;
        }
        public void ApparaillerBouton()
        {
            if (this.GetStatus() == false)
            {
                this.bouton.Content = this.imageDos;
            } else
            {
                this.bouton.Content = this.imageFace;
            }
        }
        public string Description()
        {
            return $"+--------+ Description +--------+\nNombre de Carte : {nbreCarte}\nStatus : {this.GetStatus()}\nValeur : {this.GetValeur()}\nTitre : {this.titre}\n+------------------------------------+";
        }
    }
}
