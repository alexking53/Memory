﻿using System;
using System.Collections.Generic;
using System.Text;
using Prototype.Service;

namespace Prototype.Model
{
    public class Partie {
        private Niveau niveau = null;
        private Personne personne= null;
        private Carte[] lesCartes = null;
        private int nbreClics = 0;
        private string status = "En cours";

        public Partie(Niveau leNiveau, Personne laPersonne, Carte[] lesCartesModeles)
        {
            int total;
            this.niveau = leNiveau;
            this.personne = laPersonne;
            total = leNiveau.GetNbreTotalCarte();
            this.lesCartes = new Carte[total];
            for (int i = 0; i< total / 2; i++)
            {
                lesCartes[i] = lesCartesModeles[i];
                lesCartes[i + (total / 2)] = new Carte(lesCartesModeles[i]);
            }
            lesCartes = LesServices<Carte>.MelangerLesCartes(lesCartes);
        }

        public int GetNbreClics()
        {
            return nbreClics;
        }
        public void SetNbreClics(int nbre)
        {
            
        }
        public string GetStatus()
        {
            return status;
        }
        public void SetStatus(string leStatus)
        {

        }
        public Carte[] GetLesCartes()
        {
            return lesCartes;
        }
        public Personne GetPersonne()
        {
            return personne;
        }
        public Niveau GetNiveau()
        {
            return niveau;
        }

        public string Description()
        {
            string result;
            result = $"La partie comprend{this.niveau.Description("")} et {this.personne.Description()} avec les cartes suivantes";
                foreach(Carte elem in this.lesCartes)
            {
                result += elem.Description();
            }
            return result;
        }
    }

 
}
