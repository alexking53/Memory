﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Model
{
    public class Niveau
    {
        private int numero = 0;
        private int nbreTotalCarte = 0;
        private int nbreColRang = 0;
        private int dimension = 0;

        public Niveau (int leNumero, int leNbreTotalCarte, int leNbreColRang, int laDimension)
        {
            this.numero = leNumero;
            this.nbreTotalCarte = leNbreTotalCarte;
            this.nbreColRang = leNbreColRang;
            this.dimension = laDimension;
        }

        public int GetNumero()
        {
            return numero;
        }
        public int GetNbreTotalCarte()
        {
            return nbreTotalCarte;
        }
        public int GetNbreColRang()
        {
            return nbreColRang;
        }
        public int GetDimension()
        {
            return dimension;
        }

        public string Description(string result)
        {
            result = $"Le niveau a \n pour valeur \n le numero{this.numero}, \n le nombre total de cartes {this.nbreTotalCarte} " +
                $"\n le nombre de rangées et de colonnes {this.nbreColRang}" +
                $"\n et une dimension de rangées et de colonnes {this.dimension}";
            return result;
        }
    }
}
