﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Model
{
    public class Personne
    {
        private string login;
        private string mdp;

        public Personne(string leLogin, string leMDP)
        {
            this.login = leLogin;
            this.mdp=leMDP;
        }
        public string GetLogin()
        {
            return login;
        }
        public string GetMDP()
        {
            return mdp;
        }

        public string Description()
        {
            return " ";
        }
    }
}
