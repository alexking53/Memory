﻿using Prototype.Controler;
using Prototype.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.View
{
    /// <summary>
    /// Logique d'interaction pour VueJouer.xaml
    /// </summary>
    public partial class VueJouer : Page
    {
        private bool raffraichir = false;

        public VueJouer()
        {
            InitializeComponent();

            for (int i = 0; i < Jouer.GetPartie().GetNiveau().GetNbreColRang(); i++)
            {
                RowDefinition row = new RowDefinition();
                ColumnDefinition column = new ColumnDefinition();

                row.Height = new GridLength(Jouer.GetPartie().GetNiveau().GetDimension());
                column.Width = new GridLength(Jouer.GetPartie().GetNiveau().GetDimension());

                GdFond.RowDefinitions.Add(row);
                GdFond.ColumnDefinitions.Add(column);
            }

            for (int i = 0; i < Jouer.GetPartie().GetNiveau().GetNbreTotalCarte(); i++)
            {
                GdFond.Children.Add(Jouer.GetPartie().GetLesCartes()[i].GetBouton());
            }

            int k = 0;
            for (int i = 0; i < Jouer.GetPartie().GetNiveau().GetNbreColRang(); i++)
            {
                for (int j = 0; j < Jouer.GetPartie().GetNiveau().GetNbreColRang(); j++)
                {
                    Grid.SetRow(GdFond.Children[k], i);
                    // GdFond.Children[k].SetNumRangFenetre(i);
                    Grid.SetColumn(GdFond.Children[k], j);
                    //GdFond.Children[k].SetNumColFenetre(j);
                    k++;
                }
            }
            for (int i = 0; i < Jouer.GetPartie().GetNiveau().GetNbreTotalCarte(); i++)
            {
                Jouer.GetPartie().GetLesCartes()[i].GetBouton().Click += new RoutedEventHandler(BoutonClick);
            }
        }
        private void BoutonClick(object sender, RoutedEventArgs e)
        {
            foreach (Carte elem in Jouer.GetPartie().GetLesCartes())
            {
                if (elem.GetBouton() == sender)
                {
                    Jouer.JouerUnTour(elem);
                }
            }
        }
    }
}
