﻿using Prototype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prototype.Service
{
    public class LesServices<T>
    {
        public static Image CreerUneImage(string leChemin)
        {
            Image rendu = new Image();
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            string chemin = leChemin;
            bi.UriSource = new Uri(chemin, UriKind.Relative);
            bi.EndInit();
            rendu.Stretch = Stretch.Fill;
            rendu.Source = bi;
            return rendu;
        }
        public static T[] MelangerLesCartes(T[] lesCartes)
        {
            Random random = new Random();
            return lesCartes.OrderBy(x => random.Next()).ToArray();
        }
    }
}
